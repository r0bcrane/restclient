#!/bin/sh

## 
## Sets up and generates all of the stuff that
## automake and libtool do for us.
##

rm -f config.*
rm -f Makefile
rm -rf aclocal.m4 autom4te.cache
rm -f stamp-h1
rm -f configure
rm -rf *.in
rm -rf src/Makefile
rm -rf src/Makefile.im
rm m4/libtool.m4  m4/lt~obsolete.m4  m4/ltoptions.m4  m4/ltsugar.m4  m4/ltversion.m4
rm -rf build-aux

echo "Setting up libtool"
libtoolize --force
echo "Setting up aclocal"
aclocal -I m4
echo "Generating configure"
autoconf
echo "Generating config.h.in"
autoheader
echo "Generating Makefile.in"
automake --add-missing

./configure
